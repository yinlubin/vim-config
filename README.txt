# Intro #

This is my vim config. 
Default theme is desert. Configured with ctrlp, taglist and some more plugins. I think it's very comfortable to use.
Welcome to use it and report bugs.

# Installation #

Put the vim-config in any folder, and then source the _vimrc in your .vimrc file.
For example: put the vim-config in /home/myname/Projects/, then write "source /home/myname/Projects/vim-config/vim_init.vim" to ~/.vimrc file.

You need to put cscope and ctags commands in your path. For windows, "cscope.exe" and "ctags.exe" can be found in extras/.

<Leader> key can be configured in vim_init.vim. It is "," by default.


# Most used keymaps #

* <space> : page down
* <Leader>tn : New tab and open current file.
* <Leader>cdc : change directory to the current file
* <Leader>ldc : change directory of current window to the current file
* <Leader>tl : toggle taglist
* Ctrl-c : copy selected text

Keymaps for cscope are defined in config/cscope.vim.