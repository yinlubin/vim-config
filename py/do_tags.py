#!/usr/bin/env python
# coding=utf-8

# Create ctags file and cscope database

import vim
import thread
import os
import sys
import fnmatch

tags_filetypes = ('*.h', '*.c', '*.cpp', '*.java', '*.py')

def _write_filelist(fn):
    with open(fn, 'w') as f:
        for folderContent in os.walk('.'):
            for filename in folderContent[2]:
                if any(fnmatch.fnmatch(filename, pat) for pat in tags_filetypes):
                    f.write(os.path.join(folderContent[0], filename))
                    f.write("\n")

def do_tags():
    print "Start generating cscope db, wait a moment."
    if not os.path.exists('cscope.files'):
        _write_filelist('cscope.files')
    if not os.path.exists('cscope.out'):
        print >> sys.stderr, os.popen(r'cscope -Rbq').read()
    if os.path.exists('cscope.out'):
        vim.command(r'cs add cscope.out')
        print "Cscope connected."
    else:
        print >> sys.stderr, 'Cannot find cscope.out, please check cscope program.'
    if not os.path.exists('tags'):
        print >> sys.stderr, os.popen(r'ctags -R .').read()


def do_tags_in_new_thread():
    thread.start_new_thread(do_tags,())

