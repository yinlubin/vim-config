"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Maintainer: yinlubin
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

if has("mac")
    let g:OS_NAME = "mac"
elseif has("win32")
    let g:OS_NAME = "win32"
elseif has("unix")
    let g:OS_NAME = "unix"
else
    let g:OS_NAME = "unknown"
endif

let g:VIM_CONFIG_PATH = matchstr(expand("<sfile>"), "^.*[\\/]")[0:-2]

" With a map leader it's possible to do extra key combinations
" like <leader>w saves the current file
let mapleader = ","
let g:mapleader = ","

" Source plugins
"for s:scriptfilename in split(glob(g:VIM_CONFIG_PATH . "/plugin/*.vim"), "\n")
    "execute "source " . s:scriptfilename
"endfor
execute "set rtp^=" . g:VIM_CONFIG_PATH
execute "set rtp^=" . g:VIM_CONFIG_PATH . "/ctrlp.vim"

" Source configs
for s:scriptfilename in split(glob(g:VIM_CONFIG_PATH . "/config/*.vim"), "\n")
    execute "source " . s:scriptfilename
endfor

