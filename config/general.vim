"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Sets how many lines of history VIM has to remember
set history=300

if has("multi_byte")
	"set bomb
	set encoding=utf-8
	set termencoding=utf-8
	set fileencoding=utf-8
    set fileencodings=ucs-bom,utf-8,cp936,big5,cp932,euc-jp,euc-kr,latin1
endif

set ffs=unix,dos,mac "Default file types

" Enable filetype plugin
filetype on
filetype plugin on
filetype indent on

" Set to auto read when a file is changed from the outside
set autoread

" remeber the position of editing file
au BufReadPost * if line("'\"") > 0|if line("'\"") <= line("$")|exe("norm '\"")|else|exe "norm $"|endif|endif

set path=**

au BufRead *.k.txt set foldmethod=marker
set cryptmethod=blowfish    " strong encryption method

" Commands for occur
command -nargs=1 OC :lclose | :lv <args> % | :lopen

