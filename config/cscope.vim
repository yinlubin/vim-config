""""""""""""""""""""""""""""""
" => Cscope config
""""""""""""""""""""""""""""""

" Create cscope db and ctags, then connect to cscope
map <F3> :call Do_CsTag()<CR>
" Delete cscope db and ctags file
map <S-F3> :call Delete_tagfile()<CR>

function Do_CsTag()
    " TODO
endfunction

function Delete_tagfile()
    :cs kill -1
    if filereadable("tags")
        call delete("tags")
    endif
    if filereadable("cscope.files")
        call delete("cscope.files")
    endif
    if filereadable("cscope.out")
        call delete("cscope.out")
    endif
endfunction
set cscopetag

if has('cscope')
    set cscopetag cscopeverbose

    if has('quickfix')
        set cscopequickfix=s-,c-,d-,i-,t-,e-
    endif

    cnoreabbrev <expr> csa
                \ ((getcmdtype() == ':' && getcmdpos() <= 4)? 'cs add'  : 'csa')
    cnoreabbrev <expr> csf
                \ ((getcmdtype() == ':' && getcmdpos() <= 4)? 'cs find' : 'csf')
    cnoreabbrev <expr> csk
                \ ((getcmdtype() == ':' && getcmdpos() <= 4)? 'cs kill' : 'csk')
    cnoreabbrev <expr> csr
                \ ((getcmdtype() == ':' && getcmdpos() <= 4)? 'cs reset' : 'csr')
    cnoreabbrev <expr> css
                \ ((getcmdtype() == ':' && getcmdpos() <= 4)? 'cs show' : 'css')
    cnoreabbrev <expr> csh
                \ ((getcmdtype() == ':' && getcmdpos() <= 4)? 'cs help' : 'csh')

    command -nargs=0 Cscope cs add $VIMSRC/src/cscope.out $VIMSRC/src
endif

