""""""""""""""""""""""""""""""
" => Config plugins
""""""""""""""""""""""""""""""

" for taglist
let g:Tlist_Show_One_File = 1
nmap <leader>tl :Tlist<cr>

" NERDTres
nmap <leader>nt :NERDTreeToggle<cr>

