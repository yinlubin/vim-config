""""""""""""""""""""""""""""""
" => Compile and run 
""""""""""""""""""""""""""""""

" <F7> -- Compile
" <F8> -- Run
function JavaEnvironment()
	nmap <F7> :w<CR>:!javac %<CR>
	nmap <F8> :execute "!java " . strpart(expand("%"), 0, len(expand("%"))-5)<CR>
endfunction

function CEnvironment()
	nmap <F7> :w<CR>:!gcc -c %<CR>
endfunction

function CppEnvironment()
	nmap <F7> :w<CR>:!g++ -c %<CR>
endfunction

function PythonEnvironment()
    set expandtab
	nmap <F8> :w<CR>:!python %<CR>
endfunction

au FileType java :call JavaEnvironment()
au FileType c :call CEnvironment()
au filetype cpp :call CppEnvironment()
au filetype python :call PythonEnvironment()


