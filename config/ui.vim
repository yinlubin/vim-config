"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => VIM user interface
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Set 3 lines to the curors - when moving vertical..
set so=3

set mouse=a	"Enable mouse

set number	"Show line number

set wildmenu "Turn on WiLd menu

set ruler "Always show current position

set lazyredraw

"set cmdheight=2 "The commandbar height

"set hid "Change buffer - without saving

" Set backspace config
set backspace=eol,start,indent
set whichwrap+=<,>

"set ignorecase "Ignore case when searching

set hlsearch "Highlight search things

set incsearch "Make search act like search in modern browsers

set magic "Set magic on, for regular expressions

set showmatch "Show matching bracets when text indicator is over them
"set mat=2 "How many tenths of a second to blink

" No sound on errors
set noerrorbells
set novisualbell
set t_vb=

set cursorline	"Highlight current line

"if you use vim in tty,
"'uxterm -cjk' or putty with option 'Treat CJK ambiguous characters as wide' on
"if exists("&ambiwidth")
"	set ambiwidth=double
"endif


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors and Fonts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"Enable syntax hl
syntax enable

" Set font according to system
if g:OS_NAME == "mac"
	set guifont=Monaco:h13
	set guifontwide=Monaco:h14
	set shell=/bin/bash
elseif g:OS_NAME == "win32"
	set gfn=Bitstream\ Vera\ Sans\ Mono:h10
elseif g:OS_NAME == "unix"
	set gfn=Monospace\ 10
	set shell=/bin/bash
endif

if has("gui_running")
	set guioptions-=T
	"set background=dark
	"set t_Co=256
	"set background=dark
	colorscheme desert

	set langmenu=en
	if g:OS_NAME == "win32"
		language message en
	endif
	source $VIMRUNTIME/delmenu.vim
	source $VIMRUNTIME/menu.vim 

	win 150 38
else
	"colorscheme zellner
	"set background=dark
endif

"lang en_US


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text, tab and indent related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

set autoindent
set smartindent "Smart indet
set wrap "Wrap lines
set display=lastline,uhex

set shiftwidth=4
set softtabstop=4
set tabstop=4
set backspace=2
set smarttab
set expandtab
set showtabline=2
"set linebreak
"set textwidth=500

"Tab configuration
map <leader>tn :tabnew %<cr>


""""""""""""""""""""""""""""""
" => Statusline
""""""""""""""""""""""""""""""
" Only if there are at least two windows
set laststatus=2

" Format the statusline
set statusline=\ %F%m%r%h\ %w\ \ CWD:\ %r%{CurDir()}%h\ \ \ Line:\ %l/%L:%c


function! CurDir()
	let curdir = substitute(getcwd(), '/Users/amir/', "~/", "g")
	return curdir
endfunction


