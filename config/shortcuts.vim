""""""""""""""""""""""""""""""
" => Some shortcuts
""""""""""""""""""""""""""""""

"Switch to dir of current file
map <Leader>cdc :cd %:p:h<cr>
map <Leader>lcd :lcd %:p:h<cr>

" Map <space> to <PageDown>, it's great for reading ebooks.
map <space> <PageDown>

" Control-C copy in visual mode
vmap <C-c> "*y

