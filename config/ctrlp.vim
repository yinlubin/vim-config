let g:ctrlp_custom_ignore = {
	\ 'dir':  '\v[\/]\.(git|hg|svn)$',
	\ 'file': '\v\.(exe|so|dll|class|swp|jar|dex|png|zip|rar|apk|7z)$',
	\ 'link': 'SOME_BAD_SYMBOLIC_LINKS',
	\ }

let g:ctrlp_by_filename = 1

map <Leader>p :CtrlPMixed<cr>
map <Leader>f :CtrlPBufTag<cr>
map <Leader>s :CtrlPBufTagAll<cr>
map <Leader>d :CtrlPBufTagAll<cr><C-\>w

